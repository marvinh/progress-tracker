# Copyright note and licensing information
Copyright &copy; Chief of Zetten, 23.04.2017<br>
This library is licensed under the terms of the [LGPL, Version 3](LICENSE.txt).
A copy of the license is distributed with this software.

# Purpose
This library is intended to be used for tracking the progress of a set of tasks
and displaying it in any way the user wants it to be displayed.

# Installation
Just download the software from the
[project website](https://gitlab.com/general-purpose-libraries/progress-tracker)
and then add it to your Java project. It you are using maven, you only have to add it as a dependency:
```
<groupId>com.gitlab.marvinh</groupId>
<artifactId>progress-tracker</artifactId>
<version>1.0</version>
```

# Usage
The API contains following interfaces:
* __ProgressCollector__: The main interface of classes tracking 
the progress of a set of tasks.
* __BackgroundProgressCollector__: It has the same purpose like
__ProgressCollector__, but classes implementing this interface
are intendet to be run as a thread periodically collecting progress
data.
* __ProgressSender__: This interface have to be implemented by
each task which shall report its progress to a 
__ProgressCollector__
* __ProgressReceiver__: This interface have to be implemented by each
class which shall receive notifications about the current progress
by a __ProgressCollector__

An example of how to use this library can be found in the __run()__
method of the __AbstractBackgroundProgressCollectorIT__ class

# Contact
If you wish to contact me, you can send me a message at [irc.freenode.net](irc://chiefofzetten@irc.freenode.net) or
write an email to [computer-science-blog@web.de](mailto:computer-science-blog@web.de).