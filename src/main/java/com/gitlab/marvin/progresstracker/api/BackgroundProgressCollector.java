package com.gitlab.marvin.progresstracker.api;

/**
 * Represents a thread which can externally be terminated
 */
public interface BackgroundProgressCollector extends ProgressCollector, Runnable {

    /**
     * @return Update interval of this DeamonProgressCollector in milliseconds.
     */
    long getUpdateIntervalMillis();

    /**
     * Set update interval of this DeamonProgressCollector in milliseconds.
     * @param updateIntervalMillis Update interval to set
     */
    void setUpdateIntervalMillis(long updateIntervalMillis);
}
