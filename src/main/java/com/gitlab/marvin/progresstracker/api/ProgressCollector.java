package com.gitlab.marvin.progresstracker.api;

import com.gitlab.marvin.progresstracker.utils.Progress;

/**
 * This interface describes objects receiving progress updates,
 * calculating total progress and reporting it to the registered
 * ProgressReceivers.
 */
public interface ProgressCollector {
    /**
     * Called by a ProgressSender to notice this ProgressCollector
     * about its current progress.
     * @param sender Sender of the progress update
     * @param progress Total progress of the sender
     */
    void reportProgress(ProgressSender sender, Progress progress);

    /**
     * Registers a ProgressReceiver which will receive progress update
     * messages since this function finishes
     * @param receiver Receiver to report the progress to
     */
    void registerProgressReceiver(ProgressReceiver receiver);

    /**
     * Unregisters a ProgressReceiver which will not receive progress update
     * messages any more since this function finishes
     * @param receiver Receiver to be unregistered
     */
    void unregisterProgressReceiver(ProgressReceiver receiver);

    /**
     * Sends current progress to all registered receivers
     */
    void sendProgress();
}
