package com.gitlab.marvin.progresstracker.api;

import com.gitlab.marvin.progresstracker.utils.Progress;

/**
 * Interface having to be implemented by each algorithm being able to report its progress
 */
public interface ProgressSender {
    /**
     * Registers a ProgressCollector which from the point of calling this method will
     * receive the current progress of this function.
     * @param progressCollector ProgressCollector which shall receive the current progress
     */
    void registerProgressCollector(ProgressCollector progressCollector);

    /**
     * Undo the registration of a progressCollector. From the point of calling this function
     * it will not receive the current progress anymore.
     * @param progressCollector ProgressWatcher to unregister
     */
    void unregisterProgressCollector(ProgressCollector progressCollector);

    /**
     * Sends the current progress to all registered ProgressCollectors
     * @param progress Progress of this algorithm
     */
    void sendProgress(Progress progress);
}
