package com.gitlab.marvin.progresstracker.utils;

import org.junit.Test;

import java.math.BigInteger;

import static org.junit.Assert.assertEquals;

/**
 * Created by chief on 19.04.17.
 */
public class ProgressTest {
    @Test
    public void getProgress() {
        for (short a = 0; a <= 1000; a++){
            assertEquals(new Progress(BigInteger.valueOf(a), BigInteger.valueOf(1000)).getProgress(),
                    ((double)a)/10, ((double)1)/10);
        }
    }

}