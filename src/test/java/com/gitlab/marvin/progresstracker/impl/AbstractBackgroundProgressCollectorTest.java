package com.gitlab.marvin.progresstracker.impl;

import com.gitlab.marvin.progresstracker.api.ProgressReceiver;
import com.gitlab.marvin.progresstracker.api.ProgressSender;
import com.gitlab.marvin.progresstracker.utils.Progress;
import org.junit.Before;
import org.junit.Test;

import java.math.BigInteger;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.doAnswer;
import static org.mockito.Mockito.mock;

/**
 * Created by chief on 20.04.17.
 */
public class AbstractBackgroundProgressCollectorTest {
    private final ProgressSender sender1 = mock(ProgressSender.class);
    private final ProgressSender sender2 = mock(ProgressSender.class);
    private final ProgressReceiver receiver = mock(ProgressReceiver.class);
    private final AbstractBackgroundProgressCollector progressCollector = new AbstractBackgroundProgressCollector();
    private final Progress[] progresses = new Progress[]{
            new Progress(BigInteger.valueOf(0), BigInteger.valueOf(100)),
            new Progress(BigInteger.valueOf(20), BigInteger.valueOf(100)),
            new Progress(BigInteger.valueOf(40), BigInteger.valueOf(100)),
            new Progress(BigInteger.valueOf(60), BigInteger.valueOf(100)),
            new Progress(BigInteger.valueOf(80), BigInteger.valueOf(100)),
            new Progress(BigInteger.valueOf(100), BigInteger.valueOf(100))
    };
    private Progress progressReceived;

    @Before
    public void setUp() {
        progressReceived = null;
        doAnswer(invocationOnMock -> {
            progressCollector.reportProgress(sender1, progresses[0]);
            return null;
        }).when(sender1).sendProgress(progresses[0]);
        doAnswer(invocationOnMock -> {
            progressCollector.reportProgress(sender1, progresses[1]);
            return null;
        }).when(sender1).sendProgress(progresses[1]);
        doAnswer(invocationOnMock -> {
            progressCollector.reportProgress(sender1, progresses[2]);
            return null;
        }).when(sender1).sendProgress(progresses[2]);
        doAnswer(invocationOnMock -> {
            progressCollector.reportProgress(sender1, progresses[3]);
            return null;
        }).when(sender1).sendProgress(progresses[3]);
        doAnswer(invocationOnMock -> {
            progressCollector.reportProgress(sender1, progresses[4]);
            return null;
        }).when(sender1).sendProgress(progresses[4]);
        doAnswer(invocationOnMock -> {
            progressCollector.reportProgress(sender1, progresses[5]);
            return null;
        }).when(sender1).sendProgress(progresses[5]);

        doAnswer(invocationOnMock -> {
            progressCollector.reportProgress(sender2, progresses[0]);
            return null;
        }).when(sender2).sendProgress(progresses[0]);
        doAnswer(invocationOnMock -> {
            progressCollector.reportProgress(sender2, progresses[1]);
            return null;
        }).when(sender2).sendProgress(progresses[1]);
        doAnswer(invocationOnMock -> {
            progressCollector.reportProgress(sender2, progresses[2]);
            return null;
        }).when(sender2).sendProgress(progresses[2]);
        doAnswer(invocationOnMock -> {
            progressCollector.reportProgress(sender2, progresses[3]);
            return null;
        }).when(sender2).sendProgress(progresses[3]);
        doAnswer(invocationOnMock -> {
            progressCollector.reportProgress(sender2, progresses[4]);
            return null;
        }).when(sender2).sendProgress(progresses[4]);
        doAnswer(invocationOnMock -> {
            progressCollector.reportProgress(sender2, progresses[5]);
            return null;
        }).when(sender2).sendProgress(progresses[5]);

        doAnswer(invocationOnMock -> {
            progressReceived = progresses[0];
            return null;
        }).when(receiver).notifiy(progresses[0]);
        doAnswer(invocationOnMock -> {
            progressReceived = progresses[1];
            return null;
        }).when(receiver).notifiy(progresses[1]);
        doAnswer(invocationOnMock -> {
            progressReceived = progresses[2];
            return null;
        }).when(receiver).notifiy(progresses[2]);
        doAnswer(invocationOnMock -> {
            progressReceived = progresses[3];
            return null;
        }).when(receiver).notifiy(progresses[3]);
        doAnswer(invocationOnMock -> {
            progressReceived = progresses[4];
            return null;
        }).when(receiver).notifiy(progresses[4]);
        doAnswer(invocationOnMock -> {
            progressReceived = progresses[5];
            return null;
        }).when(receiver).notifiy(progresses[5]);
    }

    @Test
    public void sendProgress() throws InterruptedException {
        progressCollector.registerProgressReceiver(receiver);
        for (int i = 0; i < 6; i++){
            sender1.sendProgress(progresses[i]);
            progressCollector.sendProgress();
            assertEquals(progresses[i], progressReceived);
        }
    }
}